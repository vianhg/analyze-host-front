# truora

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

This is a project made to learn Golang and vue.
The description of the project is like this:

Parte 1
Para eso, se nos pide crear un API​ rest con un endpoint que reciba un dominio
como truora.com y retorne un JSON con la siguiente información:

{
“servers”: [
{
“address”: “server1”,
“ssl_grade”: “B”,
“country”: “US”,
“owner”: “Amazon.com, Inc.”
},
{
“address”: “server2”,
“ssl_grade”: “A+”,
“country”: “US”
“owner”: “Amazon.com, Inc.”
},
{
    “address”: “server3”,
“ssl_grade”: “A”,
“country”: “US”
“owner”: “Amazon.com, Inc.”
}
],
“servers_changed”: true,
“ssl_grade”: “B”,
“previous_ssl_grade”: “A+”,
“logo”: “​https://server.com/icon.png​”,
“title”: “Title of the page”,
“is_down”: false
}

servers​ : contiene un array de los servidores asociados al dominio, cada objeto
del array contiene
- address​ : la IP o el host del servidor
- ssl_grade​ : el grado SSL calificado por SSLabs
- country​ : El país del servidor como aparece cuando se usa el comando
whois <ip>
- owner​ : la organización dueña de la IP, como aparece cuando se usa el
comando whois <ip>
servers_changed​ : es true si los servidores cambiaron, respecto a una hora o
más antes
ssl_grade​ : el grado más bajo de todos los servidores
previous_ssl_grade​ : el grado que tenía una hora o más antes
logo​ : el logo del dominio sacado del <head> del HTML
title​ : el título de la página sacado del <head> del HTML
is_down​ : true si el servidor está caído y no se puede contactar

Parte 2
Crear otro endpoint​ que liste los servidores que han sido consultados previamente.
Por ejemplo, si se consulta truora.com y google.com el endpoint debería retornar
ambos resultados:
{
“items”: [
{google.com info},
{truora.com info}
]
}

Parte 3
Crear una interfaz​ web con Vue o móvil con Android o iOS para consultar dominios
y ver las búsquedas recientes.

Fuentes de información
Información de SSL y servidores
https://api.ssllabs.com/api/v3/analyze?host=​<dominio>


Dificultades
Exportar una constante
Arreglos ni objetos son reactivos
Como recargar cuando tiene props