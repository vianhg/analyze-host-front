import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import { API_URL } from "./config";

export const ApiService = {
  init() {
    Vue.use(VueAxios, axios);
    Vue.axios.defaults.baseURL = API_URL;
  },

  query(resource, params) {
    return Vue.axios.get(resource, params).catch(error => {
      throw new Error(`[Analyze] ApiService ${error}`);
    });
  },

  get(resource) {
    return Vue.axios.get(`${resource}`);
  },

  post(resource, params) {
    return Vue.axios.post(`${resource}`, params);
  },

  update(resource, slug, params) {
    return Vue.axios.put(`${resource}/${slug}`, params);
  },

  put(resource, params) {
    return Vue.axios.put(`${resource}`, params);
  },

  delete(resource) {
    return Vue.axios.delete(resource).catch(error => {
      throw new Error(`[Analyze] ApiService ${error}`);
    });
  }
};

export const ServerService = {
  getServers() {
    this.model;
    return ApiService.get("servers");
  },
  get(host) {
    if (typeof host !== "string") {
      throw new Error("[Analyze] Analyze.get() host name is required.");
    }
    return ApiService.get(`analyze?host=${host}`);
  }
};

export default ApiService;

export const CountryService = {
  get(code) {
    if (typeof code !== "string") {
      return;
    }

    return ApiService.get(
      "https://restcountries.eu/rest/v2/alpha/" + code
    ).catch(error => {
      console.error(`[CountryService] CountryService: ${error}`);
    });
  }
};
