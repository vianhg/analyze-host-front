import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import BootstrapVue from "bootstrap-vue";

Vue.use(BootstrapVue);

import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faCoffee,
  faSearch,
  faImage,
  faNetworkWired,
  faArrowLeft,
  faThumbsDown,
  faThumbsUp
} from "@fortawesome/free-solid-svg-icons";
import {
  FontAwesomeIcon,
  FontAwesomeLayers
} from "@fortawesome/vue-fontawesome";
library.add(
  faCoffee,
  faSearch,
  faImage,
  faNetworkWired,
  faArrowLeft,
  faThumbsDown,
  faThumbsUp
);

Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.component("font-awesome-layers", FontAwesomeLayers);

import { ApiService } from "./services/api.service";

ApiService.init();

Vue.config.productionTip = false;

Vue.filter("json", function(value) {
  return JSON.stringify(value);
});

Vue.filter("boolToYesNo", function(value) {
  return value ? "Yes" : "No";
});

Vue.filter("nvlScore", function(value) {
  return value ? value : "-";
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
